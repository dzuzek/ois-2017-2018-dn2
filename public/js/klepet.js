// V tej datoteki je opredeljen objekt Klepet, 
// ki nam olajša obvladovanje funkcionalnosti uporabnika


var Klepet = function(socket) {
  this.socket = socket;
};


/**
 * Pošiljanje sporočila od uporabnika na strežnik z uporabo 
 * WebSocket tehnologije po socketu 'sporocilo'
 * 
 * @param kanal ime kanala, na katerega želimo poslati sporočilo
 * @param besedilo sporočilo, ki ga želimo posredovati
 */
Klepet.prototype.posljiSporocilo = function(kanal, besedilo) {
  var sporocilo = {
    kanal: kanal,
    besedilo: besedilo
  };
  this.socket.emit('sporocilo', sporocilo);
};


/**
 * Uporabnik želi spremeniti kanal, kjer se zahteva posreduje na strežnik 
 * z uporabo WebSocket tehnologije po socketu 'pridruzitevZahteva'
 * 
 * @param kanal ime kanala, kateremu se želimo pridružiti
 */
Klepet.prototype.spremeniKanal = function(kanal) {
  this.socket.emit('pridruzitevZahteva', {
    novKanal: kanal
  });
};


/**
 * Procesiranje ukaza, ki ga uporabnik vnese v vnosno polje, kjer je potrebno
 * najprej izluščiti za kateri ukaz gre, nato pa prebrati še parametre ukaza
 * in zahtevati izvajanje ustrezne funkcionalnosti
 * 
 * @param ukaz besedilo, ki ga uporabnik vnese v vnosni obrazec na spletu
 */
Klepet.prototype.procesirajUkaz = function(ukaz) {
  var besede = ukaz.split(' ');
  // Izlušči ukaz iz vnosnega besedila
  ukaz = besede[0].substring(1, besede[0].length).toLowerCase();
  var sporocilo = false;
  
  switch(ukaz) {
    case 'pridruzitev':
      besede.shift();
      var kanal = besede.join(' ');
      // Sproži spremembo kanala
      this.spremeniKanal(kanal);
      break;
    case 'vzdevek':
      besede.shift();
      var vzdevek = besede.join(' ');
      // Zahtevaj spremembo vzdevka
      this.socket.emit('vzdevekSpremembaZahteva', vzdevek);
      break;
    case 'zasebno':
      besede.shift();
      var besedilo = besede.join(' ');
      var parametri = besedilo.split('\"');
      var vsiUporabniki = parametri[1].split(',');//razdeli vzdevke po vejici v novo tabelo
      for(var i=0; i<vsiUporabniki.length;i++){ //postopek posiljanja priv. sp. obravnavamo za vsakega upor. posebej 
        if (vsiUporabniki[i]) {
          this.socket.emit('sporocilo', {vzdevek: vsiUporabniki[i], besedilo: parametri[3]});
          sporocilo = '(zasebno za ' + vsiUporabniki + '): ' + parametri[3]; //vsiUporabniki vrze ven kar celo tabelo
          
        } else {
          sporocilo = 'Neznan ukaz';
        }
      }
      break;
    case 'zasebnotip2':
      besede.shift();
      var besediloTip2 = besede.join(' ');
      var parametriTip2 = besediloTip2.split('\"');//loci elemente po parih narekovajev
      if(parametriTip2){
        this.socket.emit('sporocilo', {vzdevek: parametriTip2[1], besedilo: parametriTip2[3]});
        sporocilo='';
      }
      break;
      case 'barva': //sprememba barve elemntov z HTML elementov z id sporocila;kanal
        besede.shift();
        var barva = besede.join(' ');
        //this.$("#sporocila").css("color","red");
        document.getElementById("sporocila").style.color = barva;
        document.getElementById("kanal").style.color = barva;
        break;
    default:
      // Vrni napako, če pride do neznanega ukaza
      sporocilo = 'Neznan ukaz.';
      break;
  }

  return sporocilo;
};